module Api
  class BomsController < ApplicationController
    def index
      render json: Bom.all
    end

    def show
      bom = Bom.joins('left join items on items.id = bom.item_id').select("boms.*, items.name as ItemName")
      render json: {status: 'Success', message: 'Loaded', data: bom},status: :ok
    end

    def create
      # bom = Bom.new(bom_params)
      # if bom.save
      #   render json: {status: 'Success', message: 'Saved', data: bom}, status: :ok
      # else
      #   render json: {status: 'Failed', message: 'Unable to Save', data: bom.errors}, status: :unprocessable_entity
      # end   

      bom_params.each do |x|
        Bom.new(x).save
        render json: x
      end
      
    end

    private
    def bom_params
      params.permit(:parent_item_id, :item_id, :quantity)
    end
  end
end