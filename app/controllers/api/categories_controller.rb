module Api
  class CategoriesController < ApplicationController
    def index
      categories = Category.all
      # render json: {status: 'Success', message: 'Loaded', data: categories}, status: :ok
      render json: categories
    end

    def show
      # category = Category.find(params[:id])
      # render json: {status: 'Success', message: 'Loaded', data: category}, status: :ok
      render json: Category.find(params[:id])
    end
    
    def create 
      category = Category.new(category_params)

      if category.save
        render json: {status: 'Success', message: 'Saved', data: category}, status: :ok
      else
        render json: {status: 'Error', message: 'Unable to Save', data: category.errors}, status: :unprocessable_entity
      end
    end

    def destroy 
      # Category.find(params[:id]).destroy!
      category = Category.find(params[:id])
      category.destroy
      render json: {status: 'Success', message: 'Deleted', data: category}, status: :ok
    end

    def update 
      category = Category.find(params[:id])
      if category.update(category_params)
        render json: {status: 'Success', message: 'Updated', data: category}, status: :ok
      else 
        render json: {status: 'Error', message: 'Unable to Update', data: category.errors}, status: :unprocessable_entity
      end
    end


    private
    def category_params
      params.permit(:name, :description)
    end
  end  
end 