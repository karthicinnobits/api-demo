module Api
  class CustomersController < ApplicationController
    def index 
      render json: Customer.all
    end

    def show
      render json: Customer.find(params[:id])
    end

    def create
      customer = Customer.new(customer_params)

      if customer.save
        render json: {status: 'Success', message: 'Saved', data: customer}, status: :ok
      else
        render json: {status: 'Failed', message: 'Unable to Save', data: customer.errors}, status: :unprocessable_entity        
      end
    end

    def update
      customer = Customer.find(params[:id])
      if customer.update(customer_params)
        render json: {status: 'Success', message: 'Saved', data: customer}, status: :ok
      else 
        render json: {status: 'Failed', message: 'Unable to Update', data: customer.errors}, status: :unprocessable_entity        
      end
    end

    def destroy 
      customer = Customer.find(params[:id])
      customer.destroy
      render json: {status: 'Success', message: 'Deleted', data: customer}, status: :ok
    end

    private
    def customer_params
      params.permit(:name, :address, :email, :phone)
    end
  end
end