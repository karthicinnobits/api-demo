module Api
  class ItemsController < ApplicationController
    
    def index 
      items = Item.all
      # items = Item.joins('left join categories on items.category_id = categories.id').select("items.*, categories.name as CatName")
      render json: {status: 'Success', message: 'Loaded', data: items},status: :ok
    end

    def show
      render json: Item.find(params[:id])

    end

    def create 
      item = Item.new(item_params)

      if item.save
        render json: {status: 'Success', message: 'Saved', data: item}, status: :ok
      else
        render json: {status: 'Failed', message: 'Unable to Save', data: item.errors}, status: :unprocessable_entity
      end
    end

    def update
      item = Item.find(params[:id])
      if item.update(item_params)
        render json: {status: 'Success', message: 'Updated', data: item}, status: :ok
      end
    end

    def destroy
      item = Item.find(params[:id])
      item.destroy
      render json: {status: 'Success', message: 'Deleted', data: item}, status: :ok
    end
    private
    def item_params
      params.permit(:name, :category_id, stocks_attributes:[:quantity ])
    end
  end
end