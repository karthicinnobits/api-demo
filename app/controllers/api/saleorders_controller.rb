module Api
  class SaleordersController < ApplicationController
    def index
      render json: Saleorder.all
    end
    
    def show 
      # render json: Saleorder.joins('left join customers on saleorders.customer_id = customers.id').select('saleorders.*, customers.name as CustomerName').find(params[:id])
      @saleorder = Saleorder.find(params[:id])
      render json: @saleorder, include:[:saleorder_items]

    end

    def create
      saleorder = Saleorder.new(saleorder_params)
      if saleorder.save
        render json: {status: 'Success', message: 'Saved', data: saleorder}, status: :ok
      else
        render json: {status: 'Failed', message: 'Unable to Save', data: saleorder.errors}, status: :unprocessable_entity
      end
    end

    def update
      saleorder = Saleorder.find(params[:id])

      if saleorder.update(saleorder_params)
        render json: {status: 'Success', message: 'updated', data: saleorder}, status: :ok
      else
        render json: {status: 'Failed', message: 'Unable to Save', data: item.errors}, status: :unprocessable_entity
      end
    end

    def destroy 
      saleorder = Saleorder.find(params[:id])
      saleorder.destroy
      render json: {status: 'Success', message: 'Deleted', data: saleorder}, status: :ok
    end

    def bom
      @saleorder = SaleorderItem.joins('left join boms on saleorder_items.item_id = boms.parent_item_id left join stocks on saleorder_items.item_id = stocks.item_id')
      .select('saleorder_items.item_id,saleorder_items.quantity','boms.item_id as BomItem, boms.quantity as BomQuantity','stocks.quantity as InStock')
      .where(saleorder_id: params[:id])
      
      render json: @saleorder, :methods => :req_quantity

    end

    private
    def saleorder_params
      params.permit(:date, :due_date, :saleorder_number, :status, :customer_id, saleorder_items_attributes:[:id, :saleorder_id, :item_id, :quantity])
    end
  
  end
end