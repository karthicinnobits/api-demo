class Item < ApplicationRecord
  belongs_to :category
  has_many :stocks, dependent: :destroy

  accepts_nested_attributes_for :stocks, allow_destroy: true

  validates_presence_of :name
end
