class Saleorder < ApplicationRecord
  has_many :saleorder_items, dependent: :destroy
  belongs_to :customers
  has_many :boms, through: :saleorder_items
  
  accepts_nested_attributes_for :saleorder_items, reject_if: :all_blank, allow_destroy: true

end
