class SaleorderItem < ApplicationRecord
  belongs_to :saleorder
  has_many :items
  has_one :stock, through: :item
  has_many :boms, through: :item


  def req_quantity
    # self.boms.map{|s| s.quantity * s.BomQuantity}.sum
    # saleorder_items.quantity * boms.quantity
    self.quantity * self.BomQuantity
    # return "Text"
  end
end
