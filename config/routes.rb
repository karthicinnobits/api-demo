Rails.application.routes.draw do

  namespace :api do
    resources :categories

    resources :items

    resources :customers

    get 'saleorders/bom/:id', to: 'saleorders#bom', as: :saleorders
    resources :saleorders 

    resources :boms
    
  end
  # get 'api/saleorders/:id', :controller => 'saleorders', :action => 'bom'

end
