class CreateSaleorders < ActiveRecord::Migration[6.1]
  def change
    create_table :saleorders do |t|
      t.date :date
      t.date :due_date
      t.integer :saleorder_number
      t.integer :status

      t.references :customer, null: false, foreign_key: true
      t.timestamps
    end
  end
end
