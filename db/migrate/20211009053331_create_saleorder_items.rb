class CreateSaleorderItems < ActiveRecord::Migration[6.1]
  def change
    create_table :saleorder_items do |t|
      t.references :saleorder, null: false, foreign_key: true
      t.references :item, null: false, foreign_key: true
      t.integer :quantity
      
      t.timestamps
    end
  end
end
