class CreateBoms < ActiveRecord::Migration[6.1]
  def change
    create_table :boms do |t|
      t.references :parent_item, null: false, foreign_key: {to_table: :items}
      t.references :item, null: false, foreign_key: {to_table: :items}
      t.integer :quantity

      t.timestamps
    end
  end
end
