# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_10_171541) do

  create_table "boms", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "parent_item_id", null: false
    t.bigint "item_id", null: false
    t.integer "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["item_id"], name: "index_boms_on_item_id"
    t.index ["parent_item_id"], name: "index_boms_on_parent_item_id"
  end

  create_table "categories", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "customers", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "items", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.bigint "category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_items_on_category_id"
  end

  create_table "materials", charset: "utf8mb4", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "saleorder_items", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "saleorder_id", null: false
    t.bigint "item_id", null: false
    t.integer "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["item_id"], name: "index_saleorder_items_on_item_id"
    t.index ["saleorder_id"], name: "index_saleorder_items_on_saleorder_id"
  end

  create_table "saleorders", charset: "utf8mb4", force: :cascade do |t|
    t.date "date"
    t.date "due_date"
    t.integer "saleorder_number"
    t.integer "status"
    t.bigint "customer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_saleorders_on_customer_id"
  end

  create_table "stocks", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "item_id", null: false
    t.integer "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["item_id"], name: "index_stocks_on_item_id"
  end

  add_foreign_key "boms", "items"
  add_foreign_key "boms", "items", column: "parent_item_id"
  add_foreign_key "items", "categories"
  add_foreign_key "saleorder_items", "items"
  add_foreign_key "saleorder_items", "saleorders"
  add_foreign_key "saleorders", "customers"
  add_foreign_key "stocks", "items"
end
