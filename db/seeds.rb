5.times do
    Category.create({
        name: Faker::Construction.material,
        description: Faker::Lorem.sentence
    })
end